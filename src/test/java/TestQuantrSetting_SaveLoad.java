
import hk.quantr.setting.library.QuantrSettingLibrary;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestQuantrSetting_SaveLoad {

	@Test
	public void test() throws Exception {
		Person p = new Person();
		p.firstName = "Peter";
		p.lastName = "Cheung";
		p.setAge(44);
		System.out.println(p);
		QuantrSettingLibrary.save("object1", p);
		Person p2 = new Person();
		QuantrSettingLibrary.load("object1", p2);
		System.out.println(p2);
	}
}
