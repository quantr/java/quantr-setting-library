
import hk.quantr.setting.library.annotation.QuantrSetting;
import hk.quantr.setting.library.annotation.SettingElement;
import hk.quantr.setting.library.annotation.SettingInit;

/*
 * Copyright 2024 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author Peter <peter@quantr.hk>
 */
@QuantrSetting
public class Person {

	@SettingElement
	public String firstName;

	@SettingElement
	public String lastName;

	@SettingElement(key = "personAge")
	private int age;

	private String address;

	@SettingInit
	private void settingInit() {
//		System.out.println("Quantr Setting Object Init");
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Person{" + "firstName=" + firstName + ", lastName=" + lastName 
				+ ", age=" + age + ", address=" + address + '}';
	}

}
