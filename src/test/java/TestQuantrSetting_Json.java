
import hk.quantr.setting.library.QuantrSettingLibrary;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestQuantrSetting_Json {

	@Test
	public void test() throws Exception {
		Person p = new Person();
		p.firstName = "Peter";
		p.lastName = "Cheung";
		p.setAge(44);
		if (QuantrSettingLibrary.checkIfSerializable(p)) {
			QuantrSettingLibrary.callSettingInitFunction(p);
			String json = QuantrSettingLibrary.getJsonString("prefix1", p);
			System.out.println(json);
		}

	}
}
