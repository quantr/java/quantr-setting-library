
import hk.quantr.setting.library.QuantrSettingLibrary;
import hk.quantr.setting.library.annotation.QuantrSetting;
import hk.quantr.setting.library.annotation.SettingElement;
import java.util.ArrayList;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
@QuantrSetting
class Setting {

	@SettingElement
	public ArrayList<SettingData> paths = new ArrayList();
}

@QuantrSetting
class SettingData {

	@SettingElement
	public String path;

	public SettingData(String path) {
		this.path = path;
	}
}

public class TestQuantrSetting_TestArrayList {

	@Test
	public void test() throws Exception {
		Setting setting = new Setting();
		setting.paths.add(new SettingData("123"));
		setting.paths.add(new SettingData("456"));
		System.out.println(QuantrSettingLibrary.getJSon("setting", setting));
	}
}
