package hk.quantr.setting.library;

import hk.quantr.javalib.CommonLib;
import hk.quantr.setting.library.annotation.QuantrSetting;
import hk.quantr.setting.library.annotation.SettingElement;
import hk.quantr.setting.library.annotation.SettingInit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class QuantrSettingLibrary {

	public static void callSettingInitFunction(Object object) throws Exception {
		Class<?> clazz = object.getClass();
		for (Method method : clazz.getDeclaredMethods()) {
			if (method.isAnnotationPresent(SettingInit.class)) {
				method.setAccessible(true);
				method.invoke(object);
			}
		}
	}

	public static String getJsonString(String prefix, Object object) throws Exception {
		Class<?> clazz = object.getClass();
		Map<String, Object> jsonElementsMap = new HashMap<>();
		for (Field field : clazz.getDeclaredFields()) {
//			if (!Modifier.isStatic(field.getModifiers())) {
			field.setAccessible(true);
			if (field.isAnnotationPresent(SettingElement.class)) {
				Object obj = field.get(object);
//					if (field.getType().isPrimitive()) {
				jsonElementsMap.put(field.getName(), obj);
//					} else {
//						jsonElementsMap.put(field.getName(), getJsonString(field.getName(), obj));
//					}
			}

//			}
		}
//		String jsonString = jsonElementsMap.entrySet().stream().map(entry -> "\"" + entry.getKey() + "\":\"" + entry.getValue() + "\"").collect(Collectors.joining(","));
//		jsonString = "{\"" + prefix + "\":{" + jsonString + "}}";
		JSONObject parent = new JSONObject();
		JSONObject o = new JSONObject(jsonElementsMap);
		parent.put(prefix, o);
		return CommonLib.formatJson(parent.toString());
	}

	public static boolean checkIfSerializable(Object object) {
		if (Objects.isNull(object)) {
			return false;
		}
		Class<?> clazz = object.getClass();
		if (!clazz.isAnnotationPresent(QuantrSetting.class)) {
			return false;
		}
		return true;
	}

	/**
	 * Save object to json file
	 *
	 * @param prefix name of parent node in json
	 * @param obj the object you want to save to json
	 *
	 */
	public static boolean save(String prefix, Object obj) throws Exception {
		if (checkIfSerializable(obj)) {
			FileUtils.write(new File("setting.json"), getJSon(prefix, obj), "utf-8");
			return true;
		} else {
			return false;
		}
	}

	public static String getJSon(String prefix, Object obj) throws Exception {
		if (checkIfSerializable(obj)) {
			callSettingInitFunction(obj);
			String json = QuantrSettingLibrary.getJsonString(prefix, obj);
			return json;
		} else {
			return null;
		}
	}

	public static boolean load(String prefix, Object obj) throws FileNotFoundException, IOException, IllegalArgumentException, IllegalAccessException {
		File file = new File("setting.json");
		if (checkIfSerializable(obj) && file.exists()) {
			String jsonString = IOUtils.toString(new FileInputStream(file), "utf-8");
			JSONObject json = new JSONObject(jsonString);
			JSONObject p = json.getJSONObject(prefix);
			Class<?> clazz = obj.getClass();
			for (Field field : clazz.getDeclaredFields()) {
				field.setAccessible(true);
				if (field.isAnnotationPresent(SettingElement.class)) {
					System.out.println(field.getName() + " = " + p.get(field.getName()));
					field.set(obj, p.get(field.getName()));
				}
			}
			return true;
		} else {
			return false;
		}
	}
}
