# Quantr Setting Library

A library to save/load object to json

Project website : https://www.quantr.foundation/project/?project=Quantr%20Setting%20Library

## developer

Peter <peter@quantr.hk> System architect from Quantr

![](https://www.quantr.foundation/wp-content/uploads/2024/02/Quantr-Setting-Library.png)

